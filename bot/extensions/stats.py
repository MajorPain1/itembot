from typing import List, Optional
from fuzzywuzzy import process, fuzz
from operator import itemgetter

import discord
from discord import app_commands
from discord.ext import commands
from loguru import logger

from ..database import StatObject
from .. import TheBot, database, emojis
from ..menus import ItemView


FIND_ITEM_QUERY = """
SELECT * FROM items
INNER JOIN locale_en ON locale_en.id == items.name
WHERE locale_en.data == ? COLLATE NOCASE
"""

FIND_SET_QUERY = """
SELECT * FROM set_bonuses
"""

SET_BONUS_NAME_QUERY = """
SELECT locale_en.data FROM set_bonuses
INNER JOIN locale_en ON locale_en.id == set_bonuses.name
WHERE set_bonuses.id == ?
"""

SPELL_NAME_ID_QUERY = """
SELECT locale_en.data FROM spells
INNER JOIN locale_en ON locale_en.id == spells.name
WHERE spells.template_id == ?
"""


class Stats(commands.GroupCog, name="item"):
    def __init__(self, bot: TheBot):
        self.bot = bot

    async def fetch_item(self, name: str) -> List[tuple]:
        async with self.bot.db.execute(FIND_ITEM_QUERY, (name,)) as cursor:
            return await cursor.fetchall()

    async def fetch_set_bonus_name(self, set_id: int) -> Optional[tuple]:
        async with self.bot.db.execute(SET_BONUS_NAME_QUERY, (set_id,)) as cursor:
            return (await cursor.fetchone())[0]

    async def fetch_item_stats(self, item: int) -> List[str]:
        stats = []

        async with self.bot.db.execute(
            "SELECT * FROM item_stats WHERE item == ?", (item,)
        ) as cursor:
            async for row in cursor:
                a = row[3]
                b = row[4]

                match row[2]:
                    # Regular stat
                    case 1:
                        order, stat = database.translate_stat(a)
                        rounded_value = round(database.unpack_stat_value(b), 2)
                        stats.append(StatObject(order, int(rounded_value), stat))

                    # Starting pips
                    case 2:
                        if a != 0:
                            stats.append(StatObject(131, a, f" {emojis.PIP}"))
                        if b != 0:
                            stats.append(StatObject(132, b, f" {emojis.POWER_PIP}"))
                    
                    # Itemcards
                    case 3:
                        async with self.bot.db.execute(SPELL_NAME_ID_QUERY, (a,)) as cursor:
                            card_name = (await cursor.fetchone())[0]
                        copies = b
                        stats.append(StatObject(150, 0, f"Gives {copies} {card_name}"))
                    
                    # Maycasts
                    case 4:
                        async with self.bot.db.execute(SPELL_NAME_ID_QUERY, (a,)) as cursor:
                            card_name = (await cursor.fetchone())[0]
                        stats.append(StatObject(151, 0, f"Maycasts {card_name}"))

                    # Speed bonus
                    case 5:
                        stats.append(StatObject(133, a, f"% {emojis.SPEED}"))

                    # Passengers
                    case 6:
                        stats.append(StatObject(132, a, f" Passenger Mount"))

        stats = sorted(stats, key=lambda stat: stat.order)
        _return_stats = []
        for raw_stat in stats:
            raw_stat: StatObject
            _return_stats.append(raw_stat.to_string())

        return _return_stats

    async def build_item_embed(self, row) -> discord.Embed:
        item_id = row[0]
        set_bonus = row[2]
        rarity = database.translate_rarity(row[3])
        jewels = row[4]
        kind = database.ItemKind(row[5])
        extra_flags = database.ExtraFlags(row[6])
        school = row[7]
        equip_level = row[8]
        min_pet_level = row[9]
        max_spells = row[10]
        max_copies = row[11]
        max_school_copies = row[12]
        deck_school = row[13]
        max_tcs = row[14]
        item_name = row[17]

        requirements = []
        if equip_level != 0:
            requirements.append(f"Level {equip_level}+")
        requirements.append(database.translate_equip_school(school))

        stats = []
        if database.ExtraFlags.PET_JEWEL in extra_flags:
            stats.append(f"Level {database.translate_pet_level(min_pet_level)}+")
        elif kind == database.ItemKind.DECK:
            stats.append(f"Max spells {max_spells}")
            stats.append(f"Max copies {max_copies}")
            stats.append(
                f"Max {database.translate_school(deck_school)} copies {max_school_copies}"
            )
            stats.append(f"Sideboard {max_tcs}")
        stats.extend(await self.fetch_item_stats(item_id))

        embed = (
            discord.Embed(
                color=database.make_school_color(school),
                description="\n".join(stats) or "\u200b",
            )
            .set_author(name=item_name, icon_url=database.get_item_icon_url(kind))
            .add_field(name="Requirements", value="\n".join(requirements))
        )

        if jewels != 0:
            embed = embed.add_field(
                name="Sockets", value=database.format_sockets(jewels)
            )

        if set_bonus != 0:
            set_name = await self.fetch_set_bonus_name(set_bonus)
            embed = embed.add_field(name="Set Bonus", value=set_name)

        emoji_flags = database.translate_flags(extra_flags)
        if len(emoji_flags) > 0:
            embed.add_field(name="Flags", value="".join(emoji_flags))

        return embed

    @app_commands.command(name="find", description="Finds a Wizard101 item by name")
    @app_commands.describe(name="The name of the item to search for")
    async def find(
        self, 
        interaction: discord.Interaction, 
        *, 
        name: str
    ):
        logger.info("Requested item '{}'", name)

        rows = await self.fetch_item(name)
        if not rows:
            closest_names = [(string, fuzz.token_set_ratio(name, string) + fuzz.ratio(name, string)) for string in self.bot.item_list]
            closest_names = sorted(closest_names, key=lambda x: x[1], reverse=True)
            closest_name = closest_names[0][0]
            logger.info("Failed to find '{}' instead searching for {}", name, closest_name)
            rows = await self.fetch_item(closest_name)

        view = ItemView([await self.build_item_embed(row) for row in rows])
        await view.start(interaction)

    @app_commands.command(name="list", description="Finds a list of item names that contain the string")
    @app_commands.describe(name="The name of the items to search for")
    async def list_names(
        self, 
        interaction: discord.Interaction, 
        *, 
        name: str,
    ):
        logger.info("Search for items that contain '{}'", name)


        items_containing_name = []
        for item in self.bot.item_list:
            item: str

            
            if name.lower() in item.lower():
                items_containing_name.append(item)

        no_duplicate_items = [*set(items_containing_name)]
        alphabetic_items = sorted(no_duplicate_items)

        if len(alphabetic_items) > 0:
            chunks = [alphabetic_items[i:i+15] for i in range(0, len(alphabetic_items), 15)]
            item_embeds = []
            for item_chunk in chunks:
                embed = (
                    discord.Embed(
                        description="\n".join(item_chunk) or "\u200b",
                    )
                    .set_author(name=f"Searching: {name}", icon_url=emojis.UNIVERSAL.url)
                )
                item_embeds.append(embed)

            view = ItemView(item_embeds)
            await view.start(interaction)

        else:
            embed = discord.Embed(description=f"No items containing {name} found.").set_author(name=f"Searching: {name}", icon_url=emojis.UNIVERSAL.url)
            res = interaction.response
            await res.send_message(embed=embed)

        
        



async def setup(bot: TheBot):
    await bot.add_cog(Stats(bot))
