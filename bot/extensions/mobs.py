from typing import List, Optional
from fuzzywuzzy import process, fuzz
from operator import itemgetter

import discord
from discord import app_commands
from discord.ext import commands
from loguru import logger

from ..database import StatObject
from .. import TheBot, database, emojis
from ..menus import ItemView


FIND_MOB_QUERY = """
SELECT * FROM mobs
INNER JOIN locale_en ON locale_en.id == mobs.name
WHERE locale_en.data == ? COLLATE NOCASE
"""

class Mobs(commands.GroupCog, name="mob"):
    def __init__(self, bot: TheBot):
        self.bot = bot

    async def fetch_mob(self, name: str) -> List[tuple]:
        async with self.bot.db.execute(FIND_MOB_QUERY, (name,)) as cursor:
            return await cursor.fetchall()

    async def fetch_mob_stats(self, mob: int) -> List[str]:
        stats = []

        async with self.bot.db.execute(
            "SELECT * FROM mob_stats WHERE mob == ?", (mob,)
        ) as cursor:
            async for row in cursor:
                a = row[3]
                b = row[4]

                match row[2]:
                    # Regular stat
                    case 1:
                        order, stat = database.translate_stat(a)
                        rounded_value = round(database.unpack_stat_value(b), 2)
                        stats.append(StatObject(order, int(rounded_value), stat))

                    # Starting pips
                    case 2:
                        if a != 0:
                            stats.append(StatObject(131, a, f"{emojis.PIP}"))
                        if b != 0:
                            stats.append(StatObject(132, b, f"{emojis.POWER_PIP}"))

                    # Speed bonus
                    case 5:
                        stats.append(StatObject(133, a, f"{emojis.SPEED}"))

        stats = sorted(stats, key=lambda stat: stat.order)

        return stats
    
    async def fetch_mob_items(self, mob: int) -> List[str]:
        items = []

        async with self.bot.db.execute(
            "SELECT * FROM mob_items WHERE mob == ?", (mob,)
        ) as cursor:
            async for row in cursor:
                items.append(row[2])

        return items


    async def build_mob_embed(self, row) -> discord.Embed:
        mob_id = row[0]
        real_name = row[2].decode("utf-8") 
        title = row[3]
        rank = row[4]
        hp = row[5]
        school = row[6]
        secondary_school = row[7]
        is_shadow = row[8]
        has_cheats = row[9]
        intelligence = row[10]
        selfishness = row[11]
        aggressiveness = row[12]
        mob_name = row[14]

        ai = [f"Intelligence {intelligence}", f"Selfishness {selfishness}", f"Aggressiveness {aggressiveness}"]

        stats = await self.fetch_mob_stats(mob_id)
        items = await self.fetch_mob_items(mob_id)
        await database.sum_stats(self.bot.db, stats, items)

        stats = sorted(stats, key=lambda stat: stat.order)
        _return_stats = []
        for stat in stats:
            _return_stats.append(stat.to_string())

        stats = _return_stats

        flags = []
        if secondary_school:
            flags.append(f"Secondary School {database.translate_school(secondary_school)}")
        if bool(has_cheats):
            flags.append("This Boss Cheats!")
        if bool(is_shadow):
            flags.append("Has Shadow")
        


        embed = (
            discord.Embed(
                color=database.make_school_color(school),
                description="\n".join(stats) or "\u200b",
            )
            .set_author(name=f"{mob_name} ({real_name})\nHP {hp}\nRank {rank} {title}", icon_url=database.translate_school(school).url)
            .add_field(name="AI", value="\n".join(ai))
        )

        if flags:
            embed.add_field(name="Flags", value="\n".join(flags))

        return embed

    @app_commands.command(name="find", description="Finds a Wizard101 mob by name")
    @app_commands.describe(name="The name of the mob to search for")
    async def find(
        self, 
        interaction: discord.Interaction, 
        *, 
        name: str
    ):
        logger.info("Requested item '{}'", name)

        rows = await self.fetch_mob(name)
        if not rows:
            closest_names = [(string, fuzz.token_set_ratio(name, string) + fuzz.ratio(name, string)) for string in self.bot.mob_list]
            closest_names = sorted(closest_names, key=lambda x: x[1], reverse=True)
            closest_name = closest_names[0][0]
            logger.info("Failed to find '{}' instead searching for {}", name, closest_name)
            rows = await self.fetch_mob(closest_name)

        view = ItemView([await self.build_mob_embed(row) for row in rows])
        await view.start(interaction)

    @app_commands.command(name="list", description="Finds a list of mob names that contain the string")
    @app_commands.describe(name="The name of the mobs to search for")
    async def list_names(
        self, 
        interaction: discord.Interaction, 
        *, 
        name: str,
    ):
        logger.info("Search for mobs that contain '{}'", name)


        mobs_containing_name = []
        for mob in self.bot.mob_list:
            mob: str

            
            if name.lower() in mob.lower():
                mobs_containing_name.append(mob)

        no_duplicate_mobs = [*set(mobs_containing_name)]
        alphabetic_mobs = sorted(no_duplicate_mobs)

        if len(alphabetic_mobs) > 0:
            chunks = [alphabetic_mobs[i:i+15] for i in range(0, len(alphabetic_mobs), 15)]
            mob_embeds = []
            for mob_chunk in chunks:
                embed = (
                    discord.Embed(
                        description="\n".join(mob_chunk) or "\u200b",
                    )
                    .set_author(name=f"Searching: {name}", icon_url=emojis.UNIVERSAL.url)
                )
                mob_embeds.append(embed)

            view = ItemView(mob_embeds)
            await view.start(interaction)

        else:
            embed = discord.Embed(description=f"No mobs containing {name} found.").set_author(name=f"Searching: {name}", icon_url=emojis.UNIVERSAL.url)
            res = interaction.response
            await res.send_message(embed=embed)

        
        



async def setup(bot: TheBot):
    await bot.add_cog(Mobs(bot))
